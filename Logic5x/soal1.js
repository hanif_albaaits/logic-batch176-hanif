require("../base/base");
var utility = require("../utility/utility");

module.exports = function soal1(req, res, next) {
  n = req.params.n1;
  nilaiTinggi = n * 2 + 1;
  nilaiLebar = n * 2 + 1;
  nilaiTengah = (nilaiLebar - 1) / 2;
  luas = (n * (n + 1)) / 2;

  Array2D = utility.SetArray(nilaiTinggi, nilaiLebar);
  IsiArray();
  utility.GenerateTableResult(Array2D, res);
};

function IsiArray() {
  let fibonaci2 = utility.ambilfibonaci2(luas);
  let ambil = utility.genap(luas);
  let fibonaci3 = utility.ambilfibonaci3(luas);
  let counter = 0;
  let fibo2 = 0;
  let fibo3 = 0;
  let genap = 0;
  let masuk = true;

  for (i = 0; i < nilaiTinggi; i++) {
    for (j = 0; j < nilaiLebar; j++) {
      if (i + j < nilaiTengah) {
        counter += 1;
        Array2D[i][j] = "@";
      } else if (j - i > nilaiTengah) Array2D[i][j] = ambil[genap++];
      else if (i - j > nilaiTengah) {
        Array2D[i][j] = fibonaci2[fibo2++];
        console.log(fibonaci2);
      } else if (i + j > (nilaiLebar - 1) * 1.5)
        Array2D[i][j] = fibonaci3[fibo3++];
      else Array2D[i][j] = "*";
    }
  }
}
