require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal3(req,res,nex){
    n = req.params.n1;
    nilaiLebar = n*2+1;
    nilaiTinggi = n*2+1;
    nilaiTengah = n;
    luas = 9;
    garis = n-2;
    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);
    isiAray();
    utility.GenerateTableResult(Array2D,res);
}

function isiAray(){
    let ambil = utility.ganjil(luas*2);
    let fibonaci2 = utility.ambilfibonaci2(luas);
    let fibonaci3 = utility.ambilfibonaci3(luas);
    let karakter = utility.ambilkarakter(garis);

    let counter =0;
    let fibo2 = 0;
    let fibo3 = 0;
    let huruf = 0;
    let huruf1 = 0;
   
    for(i=0;i<nilaiTinggi;i++){
        for(j=0;j<nilaiLebar;j++){
            if(i+j >= nilaiTengah && j-i<=nilaiTengah && i<=2)
            Array2D[i][j] = ambil[counter++]
            else if(i+j>= nilaiTengah && i-j <=nilaiTengah && j<=2)
            Array2D[i][j] = fibonaci2[fibo2++];
            else if (i-j <= nilaiTengah && i+j <=(nilaiTinggi-1)*1.5  && i>=nilaiTinggi-3)
            Array2D[i][j] = ambil[counter++]
            else if (j-i <=nilaiTengah && i+j <=(nilaiTinggi-1)*1.5 && j>=nilaiTinggi-3)
            Array2D[i][j] = fibonaci3[fibo3++]
            else if (j==n && i>=3 && i<=n)
            Array2D[i][j] = karakter[huruf++];
            else if (j==n && i<=nilaiTinggi && i>=n){
            huruf--;
            Array2D[i][j] = karakter[huruf-1];}
            else if (i==n && j>=3 && j<=n)
            Array2D[i][j] = karakter[huruf1++];
            else if (i==n && j<=nilaiTinggi && j>=n){
                huruf1--;
                Array2D[i][j] = karakter[huruf1];
            }
        }
    }
}
