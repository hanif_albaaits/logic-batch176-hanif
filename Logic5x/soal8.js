require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal8 (req,res,next){

    let n = parseInt(req.params.n1);
    nilaiLebar = 2*n-1;
    nilaiTinggi = 2*n-1;

    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);
    isiArray();
    utility.GenerateTableResult(Array2D,res);
}

function isiArray(){
    for(i=0;i<nilaiTinggi;i++){
        for(j=0;j<nilaiLebar;j++){
            if(i<j-1 && i+j <nilaiLebar-2){
                Array2D[i][j] = "A";}
            else if(i<j-1 && i+j > nilaiLebar){
                Array2D[i][j] = "B";}
            else if (i-1>j && i+j >nilaiLebar){
                Array2D[i][j] = "C";}
            else if (i-1>j && i+j < nilaiLebar-2){
                Array2D[i][j] = "D";}
            else if(i==j) {
                Array2D[i][j] = "E";
            }
            else if(i+j == nilaiLebar-1) {
                Array2D[i][j] = "F";
            }
        }
    }
}