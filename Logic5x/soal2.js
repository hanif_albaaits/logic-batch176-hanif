require("../base/base");
var utility = require("../utility/utility");

module.exports = function soal2(req, res, next) {
  n = req.params.n1;
  nilaiLebar = n * 2 + 1;
  nilaiTinggi = n * 2 + 1;
  nilaiTengah = (nilaiLebar - 1) / 2;
  luas = (n * (n + 1)) / 2;

  Array2D = utility.SetArray(nilaiTinggi, nilaiLebar);
  isiArray();
  utility.GenerateTableResult(Array2D, res);
};

function isiArray() {
  let fibonaci2 = utility.ambilfibonaci2(luas);
  let ambil = utility.genap(luas);
  let fibonaci3 = utility.ambilfibonaci3(luas);
  let counter = 1;
  let counter1 = 1;
  let fibo2 = 0;
  let fibo3 = 0;
  let genap = 0;

  for (i = 0; i < nilaiTinggi; i++) {
    for (j = 0; j < nilaiLebar; j++) {
      if (i + j < nilaiTengah) Array2D[i][j] = "mas";
      //[counter++];
      else if (j - i > nilaiTengah) Array2D[i][j] = ambil[genap++];
      else if (i - j > nilaiTengah) Array2D[i][j] = fibonaci2[fibo2++];
      else if (i + j > (nilaiTinggi - 1) * 1.5)
        Array2D[i][j] = fibonaci3[fibo3++];
      else if (
        i + j == nilaiTengah ||
        j - i == nilaiTengah ||
        i - j == nilaiTengah ||
        i + j == (nilaiTinggi - 1) * 1.5
      )
        Array2D[i][j] = "*";
    }
  }

  for (i = 0; i < nilaiTinggi; i++) {
    for (j = nilaiLebar - 1; j >= 0; j--) {
      if (
        i + j > nilaiTengah &&
        j - i < nilaiTengah &&
        i - j < nilaiTengah &&
        i + j < (nilaiTinggi - 1) * 1.5
      )
        Array2D[j][i] = [counter1++];
    }
  }
}
