const soal1 = require('./soal1');
const soal2 = require('./soal2');
const soal3 = require('./soal3');
const soal4 = require('./soal4');
const soal6 = require('./soal6');
const soal7 = require('./soal7');
const soal8 = require('./soal8');

module.exports= exports= function(server){
    server.get('/Logic5x/soal1/:n1',soal1)
    server.get('/Logic5x/soal2/:n1',soal2)
    server.get('/Logic5x/soal3/:n1',soal3)
    server.get('/Logic5x/soal4/:n1',soal4)
    server.get('/Logic5x/soal6/:n1',soal6)                                         
    server.get('/Logic5x/soal7/:n1',soal7)
    server.get('/Logic5x/soal8/:n1',soal8)
}