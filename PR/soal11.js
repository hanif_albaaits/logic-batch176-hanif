//OUTPUTNYA DIAGONAL
require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal1(req,res,next){
    n = req.params.n1;
    nilaiTinggi = n;
    nilaiLebar = n;
    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);  
    IsiArray();
    //console.log(Array2D);
    utility.GenerateTableResult(Array2D,res);
}

function IsiArray(){
    let ambil = utility.ganjil(nilaiLebar);

    for(i=0;i<nilaiTinggi;i++){
        for(j=0;j<nilaiLebar;j++){
           if (i==j) {
            Array2D[i][j] = ambil[i];
           }
        }
    }
}