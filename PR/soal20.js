//OUTPUTNYA DIAGONAL INCREMENET
require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal3(req,res,next){
    n = req.params.n1;
    nilaiTinggi = n;
    nilaiLebar = n;
    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);  
    IsiArray();
    //console.log(Array2D);
    utility.GenerateTableResult(Array2D,res);
}

function IsiArray(){
    let ambil = utility.genap(nilaiLebar);

    for(i=0;i<nilaiTinggi;i++){
        for(j=0;j<nilaiLebar;j++){
            //kotak kanan
           if (i>=(nilaiLebar-1)/2) {
            Array2D[i][j] = ambil[nilaiLebar-(i+1)];
            }
            //kotak kiri
            if (i<=(nilaiLebar-1)/2) {
                Array2D[i][j] = ambil[i];
                }
        }
    }
}