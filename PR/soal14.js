require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal3(req,res,next){
    n = req.params.n1;
    nilaiTinggi = n;
    nilaiLebar = n;
    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);  
    IsiArray();
    //console.log(Array2D);
    utility.GenerateTableResult(Array2D,res);
}

function IsiArray(){

    let ambil = utility.ganjil(nilaiLebar);
    let ambil1 = utility.genap(nilaiLebar);

    for(i=0;i<nilaiTinggi;i++){
        for(j=0;j<nilaiLebar;j++){
            //diagonal kiri
           if ((i+j)==(nilaiLebar-1)) {
            Array2D[i][j] = ambil1[nilaiLebar-(i+1)];
           }
            //diagonal kanan
            if (i==j) {
                Array2D[i][j] = ambil[i];
            }
            //nilai tengah kolom
            if ((nilaiLebar-1)/2==j) {
               Array2D[i][j] = ambil[i];
            }
            //nilai tengah baris
            if ((nilaiLebar-1)/2==i) {
                Array2D[i][j] = ambil1[j];
             }
        }
    }
}