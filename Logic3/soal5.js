require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal5(req,res,next){
    n1 = req.params.n1;
    n2 = req.params.n2;
    n3 = req.params.n3;

    nilaiTinggi = 1;
    nilaiLebar = n3;
    awal = parseInt(n1);
    kelipatan = parseInt(n2);

    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);
    IsiArray();
    //console.log(Array2D);
    utility.GenerateTableResult(Array2D,res);
}

function IsiArray(){
    let ambil = utility.kelipatan(awal,kelipatan,nilaiLebar);

    for(i=0;i<nilaiTinggi;i++){
        for(j=0;j<nilaiLebar;j++){
            Array2D[i][j] = ambil[j];
    }
}
}