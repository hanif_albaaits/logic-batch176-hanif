const restify = require('restify');
const server = restify.createServer({
    name: 'Logic web Service',
    version: '1.0.0'
});

require('./Logic1')(server);
require('./Logic2')(server);
require('./Logic3')(server);
require('./Logic4')(server);
require('./Logic5')(server);
require('./Logic5x')(server);
require('./Logic6')(server);
require('./Logic6x')(server);
require('./Logic10')(server);
require('./PR')(server);
require('./Format')(server);
require('./Ujian')(server);

server.get('/',(req,res,next) => {
    var result = "HII";
    res.writeHead(200, {
        'Content-Length':Buffer.byteLength(result),
        'Content-Type': 'text/html'
    });
    res.write(result);
    res.end();
});

const port=process.env.port || 3000
server.listen(port, function(){
    console.log('%s Listen at %s', server.name, server.url);
});