const soal1 = require('./soal1');
const soal2 = require('./soal2');
const soal3 = require('./soal3');


module.exports= exports= function(server){
    server.get('/Logic6x/soal1/:n1',soal1)
    server.get('/Logic6x/soal2/:n1',soal2)
    server.get('/Logic6x/soal3/:n1',soal3)
}
