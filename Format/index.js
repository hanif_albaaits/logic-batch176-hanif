const layout=require('./layout');
const soal1=require('./soal1');
const soal2=require('./soal2');
const soal3=require('./soal3');
const soal4=require('./soal4');
const soal5=require('./soal5');
const soal6=require('./soal6');
const soal7=require('./soal7');
const soal8=require('./soal8');
const soal9=require('./soal9');
const soal9v=require('./soal9v');
const soal10=require('./soal10');

module.exports= exports= function(server){
    server.get('/Format/layout/:n1',layout)
    server.get('/Format/soal1/:n1/:n2',soal1)
    server.get('/Format/soal2/:n1/:n2/:n3',soal2)
    server.get('/Format/soal3/:n1',soal3)
    server.get('/Format/soal4/:n1',soal4)
    server.get('/Format/soal5/:n1/:n2/:n3',soal5)
    server.get('/Format/soal6/:n1',soal6)
    server.get('/Format/soal7/:n1/:n2/:n3',soal7)    
    server.get('/Format/soal8/:n1/:n2',soal8)
    server.get('/Format/soal9v/:n1',soal9v)
    server.get('/Format/soal10/:n1',soal10)     
}

