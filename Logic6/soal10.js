require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal1(req,res,next){
    let n1 = parseInt(req.params.n1);
    let jumlahBangun = parseInt(req.params.n2);
    let awal = parseInt(req.params.n3);
    let kelipatan = parseInt(req.params.n4);

    nilaiTinggi = n1*jumlahBangun; // total tinggi layout
    nilaiLebar = n1*jumlahBangun; // total lebar layout

    let tinggi = n1; //segitiga tinggi
    let lebar = n1;  //segitiga alas
    luas = n1*jumlahBangun*4;   //nilai didalam segitiga

    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);
    isiArray(jumlahBangun,tinggi,lebar,awal,kelipatan);
    utility.GenerateTableResult(Array2D,res);
}

function isiArray(jumlahBangun,tinggi,lebar,awal,kelipatan){
    ambil = utility.kelipatan(awal,kelipatan,luas);
    geserbawah = 0; // variabel utk pindah baris
    counter = 0;

    for(bangun1=0;bangun1<jumlahBangun;bangun1++){
        geserkanan = 0; //variabel utk pindah kolom (reset lagi)
        for(bangun2 =0; bangun2<jumlahBangun;bangun2++){
            if (bangun1 == bangun2){
                bangun(tinggi,lebar);
            }
        geserkanan += lebar
        }
    geserbawah += tinggi
    }
}

function bangun(tinggi,lebar){
    for(i=0;i<tinggi;i++){
        for(j=0;j<lebar;j++){
            if ( i==0 || j==lebar-1 || j==0 ||i==tinggi-1){
                if(i%2==0)
                Array2D[i+geserbawah][j+geserkanan] = ambil[counter++];
                    else{
                            if(j==lebar-1){
                            Array2D[i+geserbawah][j+geserkanan] = ambil[counter+lebar-1-j];
                            counter+=2;
                            }
                            else if(i== lebar-1){
                            Array2D[i+geserbawah][j+geserkanan] = ambil[counter+lebar-1-j];}
                            else {
                                Array2D[i+geserbawah][j+geserkanan] = ambil[counter+1];}
                    }
            }
        }
    }
}
