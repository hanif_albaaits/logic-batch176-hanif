require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal1(req,res,next){
    let n = parseInt(req.params.n1);
    let awal = parseInt(req.params.n2);
    let kelipatan = parseInt(req.params.n3);

    nilaiTinggi = n+2; // total tinggi layout
    nilaiLebar = n/2*(5+n); // total lebar layout

    tinggi = 3; //segitiga tinggi
    lebar = 3;  //segitiga alas
    luas = nilaiLebar;

    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);
    isiArray(n,awal,kelipatan);
    utility.GenerateTableResult(Array2D,res);
}

function isiArray(jumlahBangun,awal,kelipatan){
    ambil = utility.kelipatan(awal,kelipatan,luas);    
    geserkanan = 0;
    geserbawah = jumlahBangun-1;
    for(bangun1=0;bangun1<jumlahBangun;bangun1++){
        bangun(bangun1);
        geserkanan += lebar+bangun1
        geserbawah -= 1;
    }
}

function bangun(bangun1){
    for(i=0;i<tinggi+bangun1;i++){
        for(j=0;j<lebar+bangun1;j++){      
            if (j== lebar+bangun1-1 || i== tinggi+bangun1-1 || i+j == lebar+bangun1-1)
                Array2D[i+geserbawah][j+geserkanan] = ambil[j+geserkanan];
        }
    }
}
