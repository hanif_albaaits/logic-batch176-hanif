require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal1(req,res,next){
    let n1 = parseInt(req.params.n1);
    let jumlahBangun = parseInt(req.params.n2);
    let awal = parseInt(req.params.n3);
    let kelipatan = parseInt(req.params.n4);
    
    let tinggi = n1; //segitiga tinggi
    let lebar = 2*n1-1;  //segitiga alas
    nilaiTinggi = n1*jumlahBangun; // total tinggi layout
    nilaiLebar = lebar*jumlahBangun; // total lebar layout
    luas = n1*n1*jumlahBangun;   //nilai didalam segitiga

    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);
    isiArray(jumlahBangun,tinggi,lebar,awal,kelipatan);
    utility.GenerateTableResult(Array2D,res);
}

function isiArray(jumlahBangun,tinggi,lebar,awal,kelipatan){
    geserbawah = 0; // variabel utk pindah baris
    counter = 0;
    for(bangun1=0;bangun1<jumlahBangun;bangun1++){
        geserkanan = 0; //variabel utk pindah kolom (reset lagi)
        for(bangun2 =0; bangun2<jumlahBangun;bangun2++){
            if (bangun1 == bangun2){
                bangun(tinggi,lebar,awal,kelipatan);
            }
        geserkanan += lebar
        }
    geserbawah += tinggi
    }
}

function bangun(tinggi,lebar,awal,kelipatan){
    let ambil= utility.kelipatan(awal,kelipatan,luas);
    for(i=0;i<tinggi;i++){
        for(j=0;j<lebar;j++){
            if( i+j == (lebar-1)/2 || j-i == (lebar-1)/2){
                Array2D[i+geserbawah][j+geserkanan] = ambil[counter++]; 
            }
            else if( i==tinggi-1){
                Array2D[i+geserbawah][j+geserkanan] = ambil[counter++]; 
            }
        }
    }
}