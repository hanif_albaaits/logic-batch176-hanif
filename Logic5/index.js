const soal1=require('./soal1');
const soal2=require('./soal2');
const soal3=require('./soal3');
const soal4=require('./soal4');
const soal5=require('./soal5');
const soal6=require('./soal6');
const soal7=require('./soal7');
const soal8=require('./soal8');
const soal9=require('./soal9');
const soal10=require('./soal10');


module.exports= exports= function(server){
    server.get('/Logic5/soal1/:n1/:n2',soal1)        
    server.get('/Logic5/soal2/:n1/:n2',soal2)
    server.get('/Logic5/soal3/:n1/:n2',soal3)
    server.get('/Logic5/soal4/:n1/:n2',soal4)                                                              
    server.get('/Logic5/soal5/:n1/:n2',soal5)                                                              
    server.get('/Logic5/soal6/:n1/:n2',soal6)                                                              
    server.get('/Logic5/soal7/:n1/:n2/:n3/:n4',soal7)                                                              
    server.get('/Logic5/soal8/:n1/:n2/:n3/:n4',soal8)   
    server.get('/Logic5/soal9/:n1/:n2/:n3/:n4',soal9)
    server.get('/Logic5/soal10/:n1/:n2/:n3/:n4',soal10)                                                                
}