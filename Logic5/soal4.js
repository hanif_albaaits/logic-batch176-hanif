require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal1(req,res,next){
    let n1 = parseInt(req.params.n1);
    let n2 = parseInt(req.params.n2);

    nilaiTinggi = n1;
    nilaiLebar = n1*n2;
    let width = n1;

    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);
    IsiArray(n2,width);
    //console.log(Array2D);
    utility.GenerateTableResult(Array2D,res);
}

function IsiArray(jumlahBangun,width){
   let shiftright = 0;

    for(let bangun=0;bangun<jumlahBangun;bangun++){
    
    if(bangun%4==0){    
        for (let i=0;i<nilaiTinggi;i++){
            for(let j=0;j<width;j++){
                if( (i==j) || (j==0) || (i==width-1)) {
                    Array2D[i][j+shiftright] = "*";
                }
            }
        }
    }
    if(bangun%4==1){    
        for (let i=0;i<nilaiTinggi;i++){
            for(let j=0;j<width;j++){
                if( (i+j)==width-1 || (j==0) || (i==0)) {
                Array2D[i][j+shiftright] = "B"
                }
            }
        }
    }
    if(bangun%4==2){    
        for (let i=0;i<nilaiTinggi;i++){
            for(let j=0;j<width;j++){
                if( (i==j) || (i==0) || (j==width-1)) {
                Array2D[i][j+shiftright] = "C"
                }
            }
        }
    }
    if(bangun%4==3){    
        for (let i=0;i<nilaiTinggi;i++){
            for(let j=0;j<width;j++){
                if( (i+j)==width-1 || (j==width-1) || (i==width-1)) {
                Array2D[i][j+shiftright] = "D"
                }
            }
        }
    }
    shiftright += width
    }
}