const soal1=require('./soal1');
const soal6=require('./soal6');
const soal5=require('./soal5');
const soal4=require('./soal4');
const soal9=require('./soal9');
const soal2=require('./soal2');
const soal7=require('./soal7');
const soal3=require('./soal3');
const soal8=require('./soal8');
const soal10=require('./soal10');

module.exports= exports= function(server){
    server.get('/Ujian/soal1/:n1/:n2',soal1)
    server.get('/Ujian/soal2/:n1/:n2/:n3',soal2)
    server.get('/Ujian/soal3/:n1',soal3)
    server.get('/Ujian/soal4/:n1',soal4)
    server.get('/Ujian/soal5/:n1/:n2/:n3',soal5)
    server.get('/Ujian/soal6/:n1',soal6)
    server.get('/Ujian/soal7/:n1/:n2/:n3',soal7)    
    server.get('/Ujian/soal8/:n1/:n2',soal8)
    server.get('/Ujian/soal9/:n1',soal9)
    server.get('/Ujian/soal10/:n1',soal10)     
}

