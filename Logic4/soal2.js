require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal1(req,res,next){
    n = req.params.n1;
    nilaiTinggi =n ;
    nilaiLebar = 2*n-1;
    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);  
    IsiArray();
    //console.log(Array2D);
    utility.GenerateTableResult(Array2D,res);
}

function IsiArray(){
    let nilaiTengah = (nilaiLebar-1/2);
    for(i=0; i<nilaiTinggi;i++){
        let counter = 1;
        for(j=0;j<nilaiLebar;j++){
                if(i<=j && j<nilaiTinggi)
                Array2D[i][j] = [counter++];
                if(i+j <= nilaiLebar-1 && j>=nilaiTinggi)
                Array2D[i][j] = [counter++];
        }
    }
}