const soal1=require('./soal1');
const soal2=require('./soal2');
const soal3=require('./soal3');
const soal4=require('./soal4');
const soal5=require('./soal5');
const soal6=require('./soal6');
const soal7=require('./soal7');


module.exports= exports= function(server){
    server.get('/Logic2/soal1/:n1',soal1)       
    server.get('/Logic2/soal2/:n1',soal2)       
    server.get('/Logic2/soal3/:n1',soal3)       
    server.get('/Logic2/soal4/:n1',soal4)       
    server.get('/Logic2/soal5/:n1',soal5)       
    server.get('/Logic2/soal6/:n1',soal6)       
    server.get('/Logic2/soal7/:n1',soal7)       
}