require('../base/base');
var utility = require('../utility/utility');

module.exports = function soal3(req,res,next){
    n = req.params.n1;
    nilaiTinggi = n;
    nilaiLebar = n;

    Array2D = utility.SetArray(nilaiTinggi,nilaiLebar);
    IsiArray();
    utility.GenerateTableResult(Array2D,res);
}

function IsiArray(){
    let ambil = utility.ambilfibonaci2(nilaiLebar);
    let ambil2 = utility.ambilfibonaci3(nilaiLebar);

    for(i=0;i<nilaiTinggi;i++){
        for(j=0;j<nilaiLebar;j++){
            if ((i==0)||(i==n-1)||(j==0)||(j==n-1)) {
                Array2D[i][j] = "*";
            }
            if (i==(nilaiLebar-1)/2){
                Array2D[i][j] = "*";
            }
            if(j==(nilaiLebar-1)/2){
                Array2D[i][j] = "*";
            }
            if((i<(nilaiLebar-1)/2) && (j<(nilaiLebar-1)/2) && (i>0) && (j>0)){
                Array2D[i][j] = "A";
            }
            if((i<(nilaiLebar-1)/2) && (j>(nilaiLebar-1)/2) && (i>0) && (j<(nilaiLebar-1))){
                Array2D[i][j] = "B";
            }
            if((i>(nilaiLebar-1)/2) && (j<(nilaiLebar-1)/2) && (i<(nilaiLebar-1)) && (j>0)){
                Array2D[i][j] = "D";
            }
            if((i>(nilaiLebar-1)/2) && (j>(nilaiLebar-1)/2) && (i<(nilaiLebar-1)) && (j<(nilaiLebar-1))){
                Array2D[i][j] = "C";
            }
        }
    }   
}