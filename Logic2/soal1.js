require("../base/base");
var utility = require("../utility/utility");

module.exports = function soal1(req, res, next) {
  n = req.params.n1;
  nilaiTinggi = n;
  nilaiLebar = n;
  Array2D = utility.SetArray(nilaiTinggi, nilaiLebar);
  IsiArray();
  //console.log(Array2D);
  utility.GenerateTableResult(Array2D, res);
};

function IsiArray() {
  for (i = 0; i < nilaiTinggi; i++) {
    for (j = 0; j < nilaiLebar; j++) {
      if (i == j) {
        Array2D[i][j] = "A";
      }
      if (i + j == n - 1 || i == 0 || i == n - 1 || j == 0 || j == n - 1)
        Array2D[i][j] = "*";
    }
  }
}
