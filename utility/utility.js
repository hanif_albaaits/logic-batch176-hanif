function utility() {

    this.GenerateTableResult = function (objArray, res) {
        let result = `<style>
        table{
            font-family :AudioTrackList,Helvetica,
            font-strech :normal;
            font-size: 10pt;
        }
        td{
            width:25px;
            height: 25px;
            background-color: Lightgrey;
            text-align: center;
            align-content: middle;
            border: 1px solid black;
        }
        </style>`;

        result += `
        <table>
            <tr>
            <td>b/k</td>`;

        for (let kolom = 0; kolom < objArray[0].length; kolom++) {
            result += "<td>";
            result += kolom;
            result += "</td>";
        }
        result += "</tr>";

        for (let baris = 0; baris < objArray.length; baris++) {
            result += "<tr>";
            result += '<td>' + baris + '</td>';
            for (let kolom = 0; kolom < objArray[baris].length; kolom++) {
                result += '<td>';
                result += objArray[baris][kolom]
                result += "</td>";
            }
        }

        result += '</table>';
        res.writeHead(200, {
            'Content-Length': Buffer.byteLength(result),
            'Content-Type': 'text/html'
        });
        res.write(result);
        res.end();
    }

    this.SetArray = function (tinggi, lebar) {
        let result = [];
        for (let baris = 0; baris < tinggi; baris++) {
            result.push([]);
            for (let kolom = 0; kolom < lebar; kolom++) {
                result[baris][kolom] = "";
            }
        }
        return result;
    };

    this.ambilfibonaci2 = function (n) {
        let arr = [1, 1];
        for (i = 2; i < n; i++) {
            arr.push(arr[i - 1] + arr[i - 2]);
        }
        return arr;
    };

    this.ambilfibonaci3 = function (n) {
        let arr = [1, 1, 1];
        for (i = 3; i < n; i++) {
            arr.push(arr[i - 1] + arr[i - 2] + arr[i - 3]);
        }
        return arr;
    };

   this.ambilkarakter = function (n) {
        let arr = [];
        for (i = 1; i <= n; i++) {
            arr.push(String.fromCharCode(64 + i));
        }
        return arr;
    };

    this.genap = function (n) {
        let arr = [];
        for (i = 1; i <= n; i++) {
            arr.push(i*2-2);
        }
        return arr;
    };

    this.ganjil = function (n) {
        let arr = [];
        for (i = 1; i <= n; i++) {
            arr.push(i*2-1);
        }
        return arr;
    };

    this.kelipatan = function (awal,kelipatan,n) {
        let arr = [];
        for (i = 1; i <= n; i++) {
            arr.push(awal+((kelipatan*i)-kelipatan));
        }
        return arr;
    };

    this.prima = function (n){
        let arr = [];
        let cetak=0;
        for(i=1;i<=50;i++){
            hitungbagi=0;
            for(a=1;a<=i;a++){
                if(i%a==0){
                    hitungbagi++;
                }
            }
            if(hitungbagi==2){
                arr.push(i);
                cetak++;
          }
          if(cetak==n){break;}
        }
        return arr;
    };

    
}

module.exports = new utility();

